import React from "react";
import "./newPost.css";
const NewPost = () => {
  return (
    <div className="newPost">
      <img
        className="newPostImg"
        src="https://images.pexels.com/photos/1181268/pexels-photo-1181268.jpeg?auto=compress&cs=tinysrgb&w=1260"
        alt=""
      />
      <form className="newPostForm">
        <div className="newPostFormGroup">
          <label htmlFor="fileInput">
            <i className="newPostIcon fas fa-plus"></i>
          </label>
          <input id="fileInput" type="file" style={{ display: "none" }} />
          <input
            className="newPostInput"
            placeholder="Blog Title"
            type="text"
            autoFocus={true}
          />
        </div>
        <div className="newPostFormGroup">
          <textarea
            className="newPostInput newPostText"
            placeholder="Description Here"
            type="text"
            autoFocus={true}
          />
        </div>
        <button className="newPostSubmit" type="submit">
          Publish
        </button>
      </form>
    </div>
  );
};

export default NewPost;
