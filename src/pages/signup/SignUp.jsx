import React from "react";
import "./signup.css";
import { Link } from "react-router-dom";
const SignUp = () => {
  return (
    <div className="signup">
      <span className="signupTitle">Sign Up</span>
      <form className="signupForm">
        <label>Username</label>
        <input className="signupInput" type="text" placeholder="xyz" />
        <label>Email</label>
        <input
          className="signupInput"
          type="text"
          placeholder="xyz@gmail.com"
        />
        <label>Password</label>
        <input className="signupInput" type="password" placeholder="A3H@!H#" />
        <button className="signupButton">signup</button>
      </form>
      <button className="signupLoginButton">
        <Link
          to="/login"
          style={{
            textDecoration: "none",
            color: "inherit",
          }}
        >
          Login
        </Link>
      </button>
    </div>
  );
};

export default SignUp;
