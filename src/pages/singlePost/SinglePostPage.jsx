import React from "react";
import SinglePost from "../../components/singlepost/SinglePost";
import "./singlepostpage.css";
import Sidebar from "./../../components/sidebar/Sidebar";

export default function Single() {
  return (
    <div className="single">
      <SinglePost />
      <Sidebar />
    </div>
  );
}
