import React from "react";
import "./setting.css";
import Sidebar from "./../../components/sidebar/Sidebar";
const Setting = () => {
  return (
    <div className="settings">
      <div className="settingsWrapper">
        <div className="settingsTitleandIcon">
          <span className="settingsTitleUpdate">Update Your Account</span>
          <span className="settingDelete">
            <i class=" settingsDeleteIcon fa-solid fa-trash"></i>
          </span>
        </div>
        <form className="settingsForm">
          <label>Profile Picture</label>
          <div className="settingsPP">
            <img
              src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
              alt=""
            />
            <label htmlFor="fileInput">
              <i class=" settingsPPIcon fa-solid fa-file-circle-plus"></i>
            </label>
            <input
              id="fileInput"
              type="file"
              style={{ display: "none" }}
              className="settingsPPInput"
            />
          </div>
          <label>Username</label>
          <input type="text" placeholder="Sudip Mahato" name="name" />
          <label>Email</label>
          <input
            type="email"
            placeholder="sudipmahato@gmail.com"
            name="email"
          />
          <label>Password</label>
          <input type="password" placeholder="Password" name="password" />
          <button className="settingsSubmitButton" type="submit">
            Update
          </button>
        </form>
      </div>
      <Sidebar />
    </div>
  );
};

export default Setting;
