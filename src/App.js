import "./App.css";
import Topbar from "./components/topbar/Topbar";
import Home from "./pages/homepage/Home";
import Single from "./pages/singlePost/SinglePostPage";
import NewPost from "./pages/newPost/NewPost";
import Setting from "./pages/setting/Setting";
import Login from "./pages/login/Login";
import SignUp from "./pages/signup/SignUp";

import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  const currentUser = false;
  return (
    <BrowserRouter>
      <Topbar />
      <Routes>
        <Route exact path="/" element={<Home/>} />
        <Route path="/posts" element={<Home />} />
        <Route path="/signup" element={currentUser ? <Home/> : <SignUp/>} />
        <Route path="/login" element={currentUser ? <Home/> : <Login/>} />
        <Route path="/newpost" element={currentUser ? <NewPost /> : <Login />} />
        <Route path="/post/:id" element={<Single />} />
        <Route path="/setting" element={currentUser ? <Setting /> : <SignUp />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
