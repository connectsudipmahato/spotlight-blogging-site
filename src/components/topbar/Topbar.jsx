import React from "react";
import { Link } from "react-router-dom";
import "./topbar.css";
const Topbar = () => {
  const currentUser = false;
  return (
    <div className="top">
      <div className="topLeft">
        <i className=" topLeftIcon fa-brands fa-square-facebook"></i>
        <i className=" topLeftIcon fa-brands fa-square-instagram"></i>
        <i className=" topLeftIcon fa-brands fa-square-twitter"></i>
        <i className=" topLeftIcon fa-brands fa-linkedin"></i>
      </div>
      <div className="topCenter">
        <ul className="topCenterList">
          <li className="topCenterListItem">
            <Link
              to="/"
              style={{
                textDecoration: "none",
                color: "inherit",
              }}
            >
              Home
            </Link>
          </li>
          <li className="topCenterListItem">
            <Link
              to="/newpost"
              style={{
                textDecoration: "none",
                color: "inherit",
              }}
            >
              Post
            </Link>
          </li>
          <li className="topCenterListItem">
            <Link
              to=""
              style={{
                textDecoration: "none",
                color: "inherit",
              }}
            >
              About
            </Link>
          </li>
          <li className="topCenterListItem">
            <Link
              to=""
              style={{
                textDecoration: "none",
                color: "inherit",
              }}
            >
              Contact
            </Link>
          </li>
        </ul>
      </div>
      <div className="topRight">
        {currentUser ? (
          <>
            <input className="topRightInput" type="text" placeholder="Search" />
            <Link to="/setting">
              <i class=" topRightIcon fa-solid fa-gear"></i>
            </Link>

            <Link to="/login">
              <i class=" topRightIcon fa-solid fa-arrow-right-from-bracket"></i>
            </Link>
          </>
        ) : (
          <>
            <Link
              className="topCenterListItemLogin"
              to="/login"
              style={{
                textDecoration: "none",
                color: "inherit",
              }}
            >
              LOGIN
            </Link>

            <Link
              className="topCenterListItemSignUp"
              to="/signup"
              style={{
                textDecoration: "none",
                color: "inherit",
              }}
            >
              SIGNUP
            </Link>
          </>
        )}
      </div>
    </div>
  );
};

export default Topbar;
