import React from "react";
import "./header.css";
const Header = () => {
  return (
    <div className="header">
      <div className="headerTitles">
        <span className="headerTitleLg">SpotLight</span>
        <span className="headerTitleSm">For Everyone</span>
      </div>
      <div>
        <img
          className="headerImg"
          src="https://images.pexels.com/photos/1181268/pexels-photo-1181268.jpeg?auto=compress&cs=tinysrgb&w=1260"
          alt=""
        />
      </div>
    </div>
  );
};

export default Header;
